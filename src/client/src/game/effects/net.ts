import { createEffect } from "@javelin/ecs";
import { createMessageHandler } from "@javelin/net";
import { io } from "socket.io-client";
export const useNet = createEffect(
  (world) => {
    const state = { bytes: 0 };
    const socket = io("ws://localhost:8000");

    const handler = createMessageHandler(world);

    socket.on("connect", () => {
      console.log("WE CONNECTED BOYYYYS");
      socket.emit("join", "testroom");
      socket.on("data", (data) => {
        state.bytes += data.byteLength;
        handler.push(data);
      });
      socket.on("player", (no: number) => {
        console.log("PLAYER NUMBER", no);
      });
    });

    socket.on("disconnect", () => {
      world.reset();
    });

    return () => {
      handler.system();
      return Object.assign(state, handler.useInfo());
    };
  },
  { global: true }
);

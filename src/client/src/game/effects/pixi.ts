import { createEffect } from "@javelin/ecs";
import * as PIXI from "pixi.js";
import jzon from "../../assets/char_sheet.json?url";

const setup = () => {
  console.log(PIXI.Loader.shared.resources);
  const sheet = PIXI.Loader.shared.resources[jzon].spritesheet;
  console.log(sheet?.textures);
  console.log("we done loading spritesheet");
};
//PIXI.Loader.shared.add(pnnng).load(setup);
PIXI.Loader.shared.add(jzon).load(setup);

export const usePixi = createEffect((world) => {
  const app = new PIXI.Application({
    backgroundColor: 0x555555,
  });

  app.renderer.view.style.position = "absolute";
  app.renderer.view.style.display = "block";
  // @ts-ignore
  app.renderer.autoResize = true;
  app.renderer.resize(window.innerWidth, window.innerHeight);
  window.addEventListener("resize", () => {
    app.renderer.resize(window.innerWidth, window.innerHeight);
  });
  const graphics = new PIXI.Graphics();
  graphics.beginFill(0x555555);
  app.stage.addChild(graphics);
  app.stage.scale.set(7, 7);
  app.stage.position.set(
    app.renderer.screen.width / 2,
    app.renderer.screen.height / 2
  );

  document.getElementById("game")?.append(app.view);
  function printkey(e: KeyboardEvent) {
    let move = 10;
    switch (e.key) {
      case "w":
        app.stage.position.y += move;
        break;
      case "a":
        app.stage.position.x += move;
        break;
      case "s":
        app.stage.position.y -= move;
        break;
      case "d":
        app.stage.position.x -= move;
        break;
    }
  }
  function zoom(e: WheelEvent) {
    let scale = 1 + (-0.25 * e.deltaY) / Math.abs(e.deltaY);
    app.stage.scale.x *= scale;
    app.stage.scale.y *= scale;
  }
  document.addEventListener("keydown", printkey);
  document.addEventListener("wheel", zoom);
  return () => {
    return { app, graphics };
  };
});

import { World } from "@javelin/ecs";
import * as PIXI from "pixi.js";
import { Sprite } from "pixi.js";
import { factionEntities } from "../../../../common/queries";
import jzon from "../../assets/char_sheet.json?url";
import { usePixi } from "../effects/pixi";

const spritesByEntity = new Map<number, Sprite>();
const SCALE = 16;
export const render = (world: World) => {
  const { app, graphics } = usePixi();
  let sheet = PIXI.Loader.shared.resources[jzon];
  factionEntities((e, [f, p]) => {
    if (!spritesByEntity.has(e)) {
      console.log("CREATING SPRITE");
      const sprite = new PIXI.Sprite(
        sheet.textures["body/black/idle/body_idle1.png"]
      );
      sprite.position.set(p.x * SCALE, p.y * SCALE);
      spritesByEntity.set(e, sprite);
      app.stage.addChild(sprite);
    } else {
      spritesByEntity.get(e)?.position.set(p.x * SCALE, p.y * SCALE);
    }
  });

  // renderableEntities((e, [r, p]) => {
  //   if (false) {
  //     //if (!spritesByEntity.has(e)) {
  //     console.log("CREATING SPRITE");
  //     const sprite = new PIXI.Sprite(
  //       sheet.textures["body/black/idle/body_idle1.png"]
  //     );
  //     sprite.position.set(p.x * 16, p.y * 16);
  //     app.stage.addChild(sprite);
  //   }
  // });
};

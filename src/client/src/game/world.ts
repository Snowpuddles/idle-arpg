import { createWorld } from "@javelin/ecs";
import Stats from "stats.js";
import { network } from "./systems/network";
import { render } from "./systems/render";

export const world = createWorld();
world.addSystem(render);
world.addSystem(network);

let running = true;
const step = (t: number) => {
  if (running) {
    world.step();
    requestAnimationFrame(step);
  }
};
requestAnimationFrame(step);

var stats = new Stats();
stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild(stats.dom);
function animate() {
  stats.begin();
  // monitored code goes here
  stats.end();
  requestAnimationFrame(animate);
}
requestAnimationFrame(animate);

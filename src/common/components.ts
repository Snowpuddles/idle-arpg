import { boolean, number, registerSchema, string } from "@javelin/ecs";

export const Player = {
  clientId: string,
  initialized: boolean,
};

export const Position = {
  x: number,
  y: number,
};

export const Initiative = {
  i: number,
};

export const MyTurn = {};

export const Movement = {
  x: number,
  y: number,
};

export const Attack = {
  x: number,
  y: number,
};

export const Health = {
  health: number,
};

export const FactionComponent = {
  name: { ...string, length: 16 },
};

registerSchema(Player, 1);
registerSchema(Position, 2);
registerSchema(Health, 3);
registerSchema(FactionComponent, 4);
registerSchema(Initiative, 5);
registerSchema(MyTurn, 6);
registerSchema(Movement, 7);

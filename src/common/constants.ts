export enum Faction {
  ENEMY = "ENEMY",
  PLAYER = "PLAYER",
}
type FactionResponses = { [key in Faction | "Default"]?: string };
export const FactionResponse: { [key in Faction]: FactionResponses } = {
  ENEMY: { Default: "ignore" },
  PLAYER: { Default: "ignore", ENEMY: "attack" },
};

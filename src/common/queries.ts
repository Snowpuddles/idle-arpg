import { createQuery } from "@javelin/ecs";
import {
  Attack,
  FactionComponent,
  Health,
  Initiative,
  Movement,
  MyTurn,
  Player,
  Position,
} from "./components";

export const players = createQuery(Player);
export const positions = createQuery(Position);
export const factionEntities = createQuery(FactionComponent, Position);
export const movingEntities = createQuery(Position, Movement);
export const attackingEntities = createQuery(Attack, Position);
export const healthEntities = createQuery(Position, Health);
export const initiativeEntities = createQuery(Initiative);
export const turnEntities = createQuery(MyTurn, Position);
//export const renderableEntities = createQuery(Renderable, Position);

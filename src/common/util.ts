import { ComponentOf } from "@javelin/ecs";
import { Position } from "./components";

export function positionDistance(
  x1: number,
  y1: number,
  x2: number,
  y2: number
): number {
  return Math.abs(Math.hypot(x2 - x1, y2 - y1));
}

export class Vector {
  x: number;
  y: number;
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
  normalize(factor: number = 1) {
    let len = this.length();
    this.x = (this.x * factor) / len;
    this.y = (this.y * factor) / len;
  }
  length() {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }
}

export const randInt = (max: number) => {
  return Math.floor(Math.random() * max);
};
export const randSplit = (max: number) => {
  return Math.floor(Math.random() * max - max / 2);
};

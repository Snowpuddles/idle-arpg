import { createServer } from "http";
import { Server } from "socket.io";
import { buildPlayer, buildWorld } from "./worlds";

export const server = createServer();
export const io = new Server(server, {
  cors: {
    origin: "*",
  },
});

io.on("connection", (socket) => {
  console.log("Client connected", socket.id);

  socket.on("join", (room) => {
    console.log("Client joining", room);
    const world = buildWorld(room);
    const player = buildPlayer(world, socket.id);
    socket.emit("player", player);
    socket.join(room);
  });
  socket.on("disconnect", () => {
    console.log("Client disconnected", socket.id);
  });
});

import { World } from "@javelin/ecs";
import { attackingEntities } from "../../common/queries";

export const attack = (world: World) => {
  attackingEntities((e, [a, p]) => {
    console.log(e, "attacking", p);
    world.detach(e, a);
  });
};

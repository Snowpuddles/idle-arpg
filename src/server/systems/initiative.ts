import { component, useInterval, World } from "@javelin/ecs";
import { MyTurn } from "../../common/components";
import { initiativeEntities } from "../../common/queries";

export const initiative = (world: World) => {
  const timer = useInterval(1000 / 60);
  initiativeEntities((e, [i]) => {
    if (timer) {
      i.i -= 1;
      if (i.i <= 0) {
        i.i = 10;
        world.attach(e, component(MyTurn));
      }
    }
  });
};

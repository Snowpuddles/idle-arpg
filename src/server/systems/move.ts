import { World } from "@javelin/ecs";
import { movingEntities } from "../../common/queries";

export const move = (world: World) => {
  movingEntities((e, [p, m]) => {
    p.x += m.x;
    p.y += m.y;
    world.detach(e, m);
    //console.log(e, p, "move", m);
  });
};

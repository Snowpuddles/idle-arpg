import { createQuery, useInterval, useMonitor, World } from "@javelin/ecs";
import { createMessageProducer, encode } from "@javelin/net";
import { FactionComponent, Position } from "../../common/components";
import { players } from "../../common/queries";
import { SEND_RATE } from "../env";
import { io } from "../net";

const networked = [Position, FactionComponent];
const queries = networked.map((x) => createQuery(x));

function getInitialMessage(world: World) {
  const producer = createMessageProducer();
  queries.forEach((q) => {
    q(producer.attach);
  });
  return producer.take();
}

//TODO world should be aware of roomid
export const net = (world: World) => {
  const send = useInterval((1 / SEND_RATE) * 1000);
  const producer = createMessageProducer();

  queries.forEach((q) => {
    useMonitor(q, producer.attach, producer.detach);
    q(producer.update);
  });

  if (!send) return;
  const message = producer.take();

  players((e, [p]) => {
    let packet;
    if (p.initialized) return;
    packet = getInitialMessage(world);
    p.initialized = true;
    console.log("Getting initial message for entity", e);

    if (packet) {
      //TODO Only send full payload to specific client
      io.to("testroom").emit("data", encode(packet));
    }
  });

  if (message) {
    io.to("testroom").emit("data", encode(message));
  }
};

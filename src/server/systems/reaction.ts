import { component, World } from "@javelin/ecs";
import { Attack } from "../../common/components";
import { turnEntities } from "../../common/queries";

export const reaction = (world: World) => {
  turnEntities((e, [turn, p]) => {
    const enemyWithinSight = false;
    if (enemyWithinSight) {
      //TODO
      world.attach(e, component(Attack, { x: 1, y: 1 }));
      world.detach(e, turn);
    }
  });
};

import { component, useInterval, World } from "@javelin/ecs";
import {
  FactionComponent,
  Health,
  Initiative,
  Position,
} from "../../common/components";
import { Faction } from "../../common/constants";
import { randInt } from "../../common/util";

export const spawn = (world: World) => {
  const timer = useInterval(1000);
  if (timer) {
    const enemy = world.create(
      component(Position, { x: randInt(50), y: randInt(50) }),
      component(Initiative, { i: randInt(10) }),
      component(FactionComponent, { name: Faction.ENEMY }),
      component(Health, { health: 10 })
    );
    //console.log("Creating enemy", enemy);
  }
};

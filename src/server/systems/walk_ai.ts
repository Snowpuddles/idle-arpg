import { component, World } from "@javelin/ecs";
import { Movement } from "../../common/components";
import { turnEntities } from "../../common/queries";
import { randSplit } from "../../common/util";

export const walk = (world: World) => {
  turnEntities((e, [turn, p]) => {
    world.detach(e, turn);
    world.attach(e, component(Movement, { x: randSplit(4), y: randSplit(4) }));
  });
};

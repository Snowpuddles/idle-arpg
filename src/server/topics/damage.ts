import { createTopic, World } from "@javelin/ecs";
import { Health } from "../../common/components";
import { deathTopic } from "./death";

// prettier-ignore
type DamageCommand = {
    source: number,
    entity: number,
    damage: number,
}
export const damageTopic = createTopic<DamageCommand>();
export const damage = (world: World) => {
  for (const command of damageTopic) {
    let h;
    console.log(command);
    if ((h = world.tryGet(command.entity, Health))) {
      console.log(command.entity, "took", command.damage, "damage");
      h.health -= command.damage;
      if (h.health <= 0) {
        console.log(command.source, "killed", command.entity);
        deathTopic.push({ source: command.source, entity: command.entity });
      }
    }
  }
};

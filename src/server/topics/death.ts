import { createTopic, World } from "@javelin/ecs";
import { Health, Player } from "../../common/components";

// prettier-ignore
type DeathCommand = {
    source: number,
    entity: number,
}
export const deathTopic = createTopic<DeathCommand>();
export const death = (world: World) => {
  for (const command of deathTopic) {
    let p;
    if ((p = world.tryGet(command.source, Player))) {
      console.log("player gains xp", command.source);
      world.destroy(command.entity);
      //add exp to player and party
    }
  }
};

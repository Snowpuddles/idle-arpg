import { component, createWorld, World } from "@javelin/ecs";
import { Clock, createHrtimeLoop } from "@javelin/hrtime-loop";
import {
  FactionComponent,
  Health,
  Initiative,
  Player,
  Position,
} from "../common/components";
import { Faction } from "../common/constants";
import { randInt } from "../common/util";
import { TICK_RATE } from "./env";
import { attack } from "./systems/attack";
import { initiative } from "./systems/initiative";
import { move } from "./systems/move";
import { net } from "./systems/network";
import { reaction } from "./systems/reaction";
import { spawn } from "./systems/spawn";
import { walk } from "./systems/walk_ai";
import { damageTopic } from "./topics/damage";
import { deathTopic } from "./topics/death";

export const worlds = new Map<string, World>();

export const buildWorld = (room: string): World => {
  if (worlds.has(room)) return worlds.get(room) as World;
  const world = createWorld<Clock>({
    topics: [damageTopic, deathTopic],
  });

  //Other
  world.addSystem(spawn);

  //Decision Systems
  world.addSystem(initiative);
  world.addSystem(reaction);
  world.addSystem(walk);

  //Action Systems
  world.addSystem(attack);
  world.addSystem(move);

  //Final Systems
  world.addSystem(net);

  createHrtimeLoop(world.step, (1 / TICK_RATE) * 1000).start();
  worlds.set(room, world);
  return world;
};

export const fillWorld = (world: World) => {
  const map = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  ];
  map.forEach((row: number[], rIdx) => {
    row.forEach((val, cIdx) => {
      world.create(
        component(Position, { x: rIdx, y: cIdx })
        //component(Renderable, { sheet: "Tileset-1.png" })
      );
    });
  });
};

export const buildPlayer = (world: World, clientId: string): number => {
  const entity = world.create(
    component(Player, { clientId: clientId }),
    component(Position, { x: randInt(50), y: randInt(50) }),
    component(Initiative, { i: randInt(10) }),
    component(FactionComponent, { name: Faction.PLAYER }),
    component(Health, { health: 10 })
    //component(Renderable)
  );
  return entity;
};
